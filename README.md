# How to collaborate with Martin

## Communication Style

- At work I prefer to communicate through written communications
- For social comminication I prefer to communicate verbally

## Working Style

- I benefit from work tasks being written or backed up with written communication
- I prefer to record zoom meetings so I can review them later
- I can handle multiple questions or instructions put to me at one time
- I prefer to have information or questions prior to meetings or discussions
- I work best when I can concentrate on a small number of tasks at one time, but I can handle multiple tasks too

## Working style summary

I prefer a work environment that is structured, which leans heavily into asynchronous communication. If I get handed multiple tasks I will try to sort them by priority and go throught them in that order. I enjoy synchronous social interactions but prefer asynchronous communication for work.

## Feedback

I prefer written feedback, followed up with a verbal conversation